FROM golang:1.17-alpine AS builder

LABEL maintainer = "RMS-SQUAD"

ENV CGO_ENABLED=0 \
    GOOS=linux \
    GO111MODULE=on

WORKDIR /app

COPY . .

RUN go mod download

RUN go build -o fx-modules


FROM alpine

WORKDIR /app

COPY --from=builder /app/fx-modules .

COPY --from=builder /app/.env . 

CMD ["./fx-modules"]

